"""
This class generated trades, and makes some simple decisions about buyin and selling.

for now it's going to paper trades.
"""
# import things we needz
from collections import defaultdict
import asyncio
import time


class pinky_trader:
    """Get's go.

    very helpful huh.
    """

    def __init__(self, marky_bot_obj):
        """Initialize the trader.

        later on the asset management will be moved to a
        different class.
        """
        self.marky_bot = marky_bot_obj
        self.marky_bot.trader = self

        self.MARGIN_LEVERAGE = 3.3
        self.MAKER_FEE = 0.1
        self.TAKE_FEE = 0.2

        self.available_assets = {'USD': 10000}
        self.available_margin = {symbol: amount * self.MARGIN_LEVERAGE for symbol, amount in self.available_assets.items()}
        self.marky_bot.logger.info(self.available_margin)
        self.held_assets = defaultdict(int)
        self.held_assets['USD'] = 0

        self.positions = defaultdict()
        self.orders = {}

        self.order_types = ('limit', 'market', 'stop')
        self.order_side = ('ask', 'bid')
        self.markets = ('margin', 'exchange')
        self.trading = True

        self.marky_bot.curses_wrapper.screen.buffer['render_balances'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.balances}
        self.marky_bot.curses_wrapper.screen.buffer['render_orders'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.orders}
        self.marky_bot.curses_wrapper.screen.buffer['render_positions'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.positions}

    def generate_margin_limit_order(self, type, pair, amount, price):
        self.marky_bot.logger.info('Tryng to generate margin limit order')
        split_pair = pair.split('/')
        self.marky_bot.logger.info('Split the pair: {} into {} / {}'.format(pair, split_pair[0], split_pair[1]))
        last_price = self.get_last_price(pair)
        order_cost = abs(amount * price)
        self.marky_bot.logger.info('Got order cost: {} amount: {} last price: {}'.format(order_cost, amount, last_price))

        matched_position = {}
        for position_id, position in self.positions.items():
            if (position['pair'] == pair and position['status'] == 'open'):
                matched_position = position
                break

        if (len(matched_position) > 0):
            if ((matched_position['amount'] < 0 and amount > 0) and (self.available_margin[split_pair[1]] + matched_position['cost']) > abs(order_cost)):
                self.marky_bot.logger.error('Reducing margin short position: {}'.format(matched_position))
            elif ((matched_position['amount'] > 0 and amount < 0) and (self.available_margin[split_pair[1]] + matched_position['cost']) > abs(order_cost)):
                self.marky_bot.logger.error('Reducing margin long position: {}'.format(matched_position))
        elif (self.available_margin[split_pair[1]] < abs(order_cost)):
            self.marky_bot.logger.error('Insufficient margin balance for order {} {} {}'.format(type, pair, amount))
            return False
        if (price <= self.get_last_price(pair) and amount < 0):
            self.marky_bot.logger.error('Limit order must be above ticker price {} {} {} {}'.format(type, pair, amount, price))
            return False
        elif (price >= self.get_last_price(pair) and amount > 0):
            self.marky_bot.logger.error('Limit order must be below ticker price {} {} {} {}'.format(type, pair, amount, price))
            return False

        order_id = '{}-{}-{}'.format(pair, time.time(), amount)
        self.orders[order_id] = {'pair': pair, 'amount': amount, 'price': price, 'market': 'margin',
                                 'type': 'limit', 'status': 'active', 'filled': 0, 'est_cost': order_cost, 'cost': 0}
        self.held_assets[split_pair[1]] += abs(order_cost / self.MARGIN_LEVERAGE)
        self.marky_bot.logger.info('Margin limit order {} generated successfuly!'.format(order_id))
        return order_id

    def get_last_price(self, pair):
        pair = pair.replace('/', '')
        try:
            last_price = self.marky_bot.exchange_queues['trades'][pair]['metrics']['last_trade'][3]
        except Exception:
            last_price = 0.0
            self.marky_bot.logger.error('No data on {} pair queue'.format(pair))
            pass

        return last_price

    def match_orders_to_trades(self):  # min(clamp_max, max(clamp_min, value))
        active_limits = {order_id: order for order_id, order in self.orders.items() if order['status'] == 'active' and order['type'] == 'limit'}
        for limit in active_limits.values():
            pair = limit['pair']
            split_pair = pair.split('/')
            pair = '{}{}'.format(split_pair[0], split_pair[1])
            tmp_fill = limit['filled']
            trade_price = self.marky_bot.exchange_queues['trades'][pair]['metrics']['last_trade'][3]
            trade_amount = self.marky_bot.exchange_queues['trades'][pair]['metrics']['last_trade'][2] - self.marky_bot.exchange_queues['trades'][pair]['metrics']['last_fill']  # in case this trade was used to fill another orderalready

            def fill_margin_limit_order():
                fill_added = limit['filled'] - tmp_fill  # how much asset was added to the order
                self.marky_bot.exchange_queues['trades'][pair]['metrics']['last_fill'] = fill_added
                cost_added = abs(fill_added * trade_price)
                limit['cost'] += cost_added

                self.held_assets[split_pair[1]] -= abs(cost_added / self.MARGIN_LEVERAGE)
                self.marky_bot.logger.info('Held assets: {} +-{}'.format(self.held_assets[split_pair[1]], abs(cost_added / self.MARGIN_LEVERAGE)))
                self.modify_position(self.match_order_to_position(limit), limit, fill_added, cost_added)

                if (abs(limit['filled']) == abs(limit['amount'])):
                    limit['status'] = 'filled'
                    cost_diff = (abs(limit['est_cost']) - abs(limit['cost'])) / self.MARGIN_LEVERAGE
                    self.held_assets[split_pair[1]] -= cost_diff
                    self.available_assets[split_pair[1]] += cost_diff
                    self.marky_bot.logger.info('Est cost and actual cost didnt match up {}'.format(cost_diff))

            if (limit['amount'] < 0) and (trade_price >= limit['price'] and trade_amount > 0):
                limit['filled'] = min(abs(limit['amount']), max(0, limit['filled'] + trade_amount))
                fill_margin_limit_order()
                self.marky_bot.logger.info('limit ask matched amount {} filled {} cost {}'.format(limit['amount'], limit['filled'], limit['cost']))
            elif (limit['amount'] > 0) and (trade_price <= limit['price'] and trade_amount < 0):
                limit['filled'] = min(0, max(-limit['amount'], limit['filled'] + trade_amount))
                fill_margin_limit_order()
                self.marky_bot.logger.info('limit bid matched amount {} filled {} cost {}'.format(limit['amount'], limit['filled'], limit['cost']))

    def match_order_to_position(self, order):
        matched_position = {}
        for position_id, position in self.positions.items():
            if (position['pair'] == order['pair'] and position['status'] == 'open'):
                matched_position = position
                break

        if (len(matched_position) == 0 and order['filled'] != 0):
            return self.open_position(order)
        elif (len(matched_position) > 0):
            return matched_position

    def open_position(self, order):
        position_id = '{}-{}'.format(order['pair'], time.time())
        self.positions[position_id] = {'pair': order['pair'], 'amount': 0.0, 'cost': 0.0, 'market': 'margin', 'status': 'open', 'plpercent': 0.0, 'pl': 0.0}
        return self.positions[position_id]

    def modify_position(self, position, order, amount, cost):
        split_pair = order['pair'].split('/')
        # gotta know if we're insreasing this position or reducing it
        cost_added_or_removed = cost if ((amount > 0 and position['amount'] <= 0) or (amount < 0 and position['amount'] >= 0)) else -cost

        self.available_assets[split_pair[1]] -= cost_added_or_removed / self.MARGIN_LEVERAGE
        position['cost'] += cost_added_or_removed
        position['amount'] -= amount
        if (position['amount'] == 0.0):
            position['status'] = 'closed'
            temp_balances = (self.available_assets[split_pair[1]], self.available_margin[split_pair[1]], self.held_assets[split_pair[1]])
            closed_position_cost = position['cost'] * (-1 if amount > 0 else 1)
            self.available_assets[split_pair[1]] += closed_position_cost
            self.marky_bot.logger.info('Position {} closed cost: {} balances: a {} m {} h {}'.format(position, closed_position_cost, self.available_assets[split_pair[1]], temp_balances[0], temp_balances[1], temp_balances[2]))
            return


    def cancel_order(self, order_id):
        split_pair = self.orders[order_id]['pair'].split('/')
        self.marky_bot.logger.info('Trying to cancel order: {}'.format(order_id))
        try:
            self.held_assets[split_pair[1]] -= abs(self.orders[order_id]['est_cost'] / self.MARGIN_LEVERAGE)
            self.orders[order_id]['status'] = 'closed'
        except Exception as e:
            self.marky_bot.logger.error('No such order to cancel {}. {}'.format(order_id, e))

    def update_margin_balance(self):
        for symbol, amount in self.held_assets.items():
            if (self.held_assets[symbol] < 0):
                self.available_assets[symbol] += amount
                self.held_assets[symbol] = 0
                self.marky_bot.logger.info('Held assets in negative {}'.format(amount))

        self.available_margin = {symbol: (amount * self.MARGIN_LEVERAGE) - (self.held_assets[symbol] * self.MARGIN_LEVERAGE) for symbol, amount in self.available_assets.items()}

    async def update_margin_pl(self):
        while self.trading:
            for position_id, position in self.positions.items():
                if (position['status'] == 'open'):
                    last_price = self.get_last_price(position['pair'])
                    position['pl'] = (position['cost'] - abs(position['amount'] * last_price)) * (-1 if position['amount'] > 0 else 1)
                    position['plpercent'] = (position['pl'] / position['cost'])
            await asyncio.sleep(0.05)

    async def watch_margin_balance(self):
        while self.trading:
            self.update_margin_balance()
            await asyncio.sleep(0.05)
