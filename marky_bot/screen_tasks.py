"""The screen jobs live here.

Add any number of special functions to do stuff on the
screen.
Add them to the self.screen.buffer list and they will be ran through the
draw_screen method.
"""

import curses


class screen_tasks:
    """screen jobs go here."""

    def __init__(self, screen):
        """Initialize screen tasksself.

        param: screen - the pinky_curses_wrapper.screen object
        """
        self.screen = screen

    def clear_attrs(self):
        """Run this at the end of any methods that set curses attributes."""
        self.screen.stdscr.attrset(0)

    def positions(self, **kwargs):
        """Display positions."""
        stat_str = 'no trades yet'
        title_str = 'Active positions:'

        positions = {position_id: positions for position_id, positions in self.screen.marky_bot.balance.positions.items() if positions['status'] == 'open'}

        arrow_up_symbol = "\u25b2"
        arrow_down_symbol = "\u25bc"
        # Turning on attributes for the trade
        self.screen.stdscr.attron(curses.A_BOLD)
        start_y = int(30)
        x_position = int(2)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        self.screen.stdscr.addstr(start_y, x_position, title_str)
        start_y += 1
        for position_id, position in positions.items():
            last_price = self.screen.marky_bot.exchange_queues['trades'][position['pair'].replace('/', '')]['metrics']['last_trade'][3]
            stat_str = '{} = {}: amount: {} avg. price: {} cost: {} p/l: {:+.4%} | {:+.4}$'.format(position_id, position['pair'], position['amount'], abs(position['cost'] / position['amount']), position['cost'], position['plpercent'], position['pl'])
            if (position['amount'] < 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_down_symbol)
            elif (position['amount'] > 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_up_symbol)

            start_y += 1
            self.screen.stdscr.addstr(start_y, x_position, stat_str)

        # Rendering the trade text
        self.clear_attrs()

    def orders(self, **kwargs):
        """Display balances."""
        stat_str = 'no trades yet'
        title_str = 'Active orders:'

        #  orders = {order for order in self.screen.marky_bot.order_manager.orders if order.STATUSES[2] in order.status}

        arrow_up_symbol = "\u25b2"
        arrow_down_symbol = "\u25bc"
        # Turning on attributes for the trade
        self.screen.stdscr.attron(curses.A_BOLD)
        start_y = int(11)
        x_position = int(2)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        self.screen.stdscr.addstr(start_y, x_position, title_str)
        start_y += 1
        for order in self.screen.marky_bot.order_manager.orders:
            stat_str = '{}: amount: {:.4f} price: {} filled: {} status: {} cost: {} side: {}'.format(order.pair[1], order.quantity, order.price, order.fill, order.status, order.estimated_cost, order.side)
            if (order.side == order.order_manager.SIDES[0]):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_down_symbol)
            elif (order.side == order.order_manager.SIDES[1]):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_up_symbol)

            start_y += 1
            self.screen.stdscr.addstr(start_y, x_position, stat_str)

        # Rendering the trade text
        self.clear_attrs()

    def balances(self, **kwargs):
        """Display balances."""
        stat_str = 'no balances yet'
        title_str = 'Balances:'

        arrow_up_symbol = "\u25b2"
        arrow_down_symbol = "\u25bc"
        # Turning on attributes for the trade
        self.screen.stdscr.attron(curses.A_BOLD)
        start_y = int(1)
        x_position = int(2)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        self.screen.stdscr.addstr(start_y, x_position, title_str)
        for asset_name, asset_balance in self.screen.marky_bot.balance.available_assets.items():
            stat_str = '{}: {:.4f}'.format(asset_name, asset_balance)
            if (asset_balance < 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_down_symbol)
            elif (asset_balance > 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_up_symbol)
            elif (asset_balance == 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])

            start_y += 1
            self.screen.stdscr.addstr(start_y, x_position, stat_str)

        start_y += 2
        title_str = 'Margin:'
        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        self.screen.stdscr.addstr(start_y, x_position, title_str)
        for asset_name, asset_balance in self.screen.marky_bot.balance.margin.tradable_margin.items():
            stat_str = '{}: {:.4f}'.format(asset_name, asset_balance)
            if (asset_balance < 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_down_symbol)
            elif (asset_balance > 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
                stat_str = '{} {}'.format(stat_str, arrow_up_symbol)
            elif (asset_balance == 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
            start_y += 1
            self.screen.stdscr.addstr(start_y, x_position, stat_str)

        start_y += 1
        self.screen.stdscr.addstr(start_y, x_position, 'Margin on orders: {}'.format(self.screen.marky_bot.order_manager.margin_on_order()))

        # Rendering the trade text
        self.clear_attrs()

    def trades_per_second(self, **kwargs):
        """Display last trade."""
        stat_str = 'no trades yet'
        title_str = 'Trades per ssecond'
        title_str1 = 'Volume per ssecond'
        metrics = {}
        if kwargs is not None:
            try:
                metrics = kwargs['metrics']
            except KeyError:
                pass
        arrow_up_symbol = "\u25b2"
        arrow_down_symbol = "\u25bc"
        # Turning on attributes for the trade
        self.screen.stdscr.attron(curses.A_BOLD)
        start_y = int(4)
        x_position = int(self.screen.width - 25)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        self.screen.stdscr.addstr(start_y, x_position - (len(title_str)//2), title_str)
        stat_str = '{:.4f}'.format(metrics['trades_per_second'])
        self.screen.stdscr.addstr(start_y+2, x_position - (len(stat_str)//2), stat_str)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
        stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, metrics['buys_per_second'], arrow_up_symbol)
        self.screen.stdscr.addstr(start_y+4, x_position - (len(stat_str)//2), stat_str)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
        stat_str = '{} {:+.4f} {}'.format(arrow_down_symbol, metrics['sells_per_second'], arrow_down_symbol)
        self.screen.stdscr.addstr(start_y+5, x_position - (len(stat_str)//2), stat_str)

        stat = abs(metrics['buys_per_second']) - abs(metrics['sells_per_second'])
        if (stat < 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
            stat_str = '{} {:.4f} {}'.format(arrow_down_symbol, stat, arrow_down_symbol)
        elif (stat > 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
            stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, stat, arrow_up_symbol)
        elif (stat == 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
            stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, stat, arrow_down_symbol)
        self.screen.stdscr.addstr(start_y+7, x_position - (len(stat_str)//2), stat_str)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        self.screen.stdscr.addstr(start_y+10, x_position - (len(title_str1)//2), title_str1)
        stat_str = '{:.4f}'.format(metrics['volume_per_second'])
        self.screen.stdscr.addstr(start_y+12, x_position - (len(stat_str)//2), stat_str)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
        stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, metrics['buy_volume_per_second'], arrow_up_symbol)
        self.screen.stdscr.addstr(start_y+14, x_position - (len(stat_str)//2), stat_str)

        self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
        stat_str = '{} {:+.4f} {}'.format(arrow_down_symbol, metrics['sell_volume_per_second'], arrow_down_symbol)
        self.screen.stdscr.addstr(start_y+15, x_position - (len(stat_str)//2), stat_str)

        stat = abs(metrics['buy_volume_per_second']) - abs(metrics['sell_volume_per_second'])
        if (stat < 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
            stat_str = '{} {:.4f} {}'.format(arrow_down_symbol, stat, arrow_down_symbol)
        elif (stat > 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
            stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, stat, arrow_up_symbol)
        elif (stat == 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
            stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, stat, arrow_down_symbol)
        self.screen.stdscr.addstr(start_y+17, x_position - (len(stat_str)//2), stat_str)

        # Rendering the trade text
        self.clear_attrs()

    def volume_stats(self, **kwargs):
        """Display last trade."""
        stat_str = 'no trades yet'
        volume_stats = {}
        if kwargs is not None:
            try:
                volume_stats = kwargs['volume_stats']
            except KeyError:
                pass
        arrow_up_symbol = "\u25b2"
        arrow_down_symbol = "\u25bc"
        # Turning on attributes for the trade
        self.screen.stdscr.attron(curses.A_BOLD)
        start_y = int(8)
        for stat_name, stat in volume_stats.items():
            x_position = int((self.screen.width // 2) - (len(stat_name) // 2) - len(stat_name) % 2)
            self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
            self.screen.stdscr.addstr(start_y, x_position, stat_name)
            if (stat < 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
                stat_str = '{} {:.4f} {}'.format(arrow_down_symbol, stat, arrow_down_symbol)
            elif (stat > 0):
                self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
                stat_str = '{} {:+.4f} {}'.format(arrow_up_symbol, stat, arrow_up_symbol)

            x_position = int((self.screen.width // 2) - (len(stat_str) // 2) - len(stat_str) % 2)
            self.screen.stdscr.addstr(start_y+1, x_position, stat_str)
            start_y = start_y + 3
        # Rendering the trade text
        self.clear_attrs()

    def last_trade(self, **kwargs):
        """Display last trade."""
        last_trade_str = 'no trades yet'
        if kwargs is not None:
            try:
                last_trade = kwargs['last_trade']
            except KeyError:
                last_trade = [0, 0, 0, 0]
                pass
        arrow_up_symbol = "\u25b2"
        arrow_down_symbol = "\u25bc"
        # Turning on attributes for the trade
        self.screen.stdscr.attron(self.screen.curses_color_pairs['white_on_black'])
        if (last_trade[2] != 0):
            last_trade_str = 'price: {:.2f} volume: {:+.2f}'.format(last_trade[3], last_trade[2])
            self.clear_attrs()
        if (last_trade[2] < 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
            last_trade_str = '{} {} {}'.format(arrow_down_symbol, last_trade_str, arrow_down_symbol)
        if (last_trade[2] > 0):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['green_on_black'])
            last_trade_str = '{} {} {}'.format(arrow_up_symbol, last_trade_str, arrow_up_symbol)

        self.screen.stdscr.attron(curses.A_BOLD)
        start_x_trade = int((self.screen.width // 2) - (len(last_trade_str) // 2) - len(last_trade_str) % 2)
        start_y = int(6)
        # Rendering the trade text
        self.screen.stdscr.addstr(start_y, start_x_trade, last_trade_str)
        self.clear_attrs()

    def title(self, **kwargs):
        """Generate the main title."""
        if kwargs is not None:
            try:
                title_text = kwargs['title_text']
            except KeyError:
                title_text = 'Default title'
                pass

        start_x_title = int((self.screen.width // 2) - (len(title_text) // 2) - len(title_text) % 2)
        start_y = int(2)
        # Turning on attributes for title
        self.screen.stdscr.attron(self.screen.curses_color_pairs['red_on_black'])
        self.screen.stdscr.attron(curses.A_BOLD)
        # Rendering the title text
        self.screen.stdscr.addstr(start_y, start_x_title, title_text)
        self.clear_attrs()

    def status_bar(self, **kwargs):
        """Generate a status bar to render."""
        ld = []
        last_debug = ''
        exchange_connection = 'Exchange disconnected \u2639'
        if (self.screen.marky_bot.wss.conn.connected.is_set()):
            exchange_connection = 'Connected to exchange \u2622'

        self.screen.marky_bot.debug_stream.seek(0)
        ld = self.screen.marky_bot.debug_stream.readlines()
        if len(ld) > 0:
            last_debug = ld[-1][:-1]
        last_debug = last_debug[:75]

        statusbarstr = "'q' to exit | 'c' connect | 'd' to disconnect | {}".format(last_debug)

        self.screen.stdscr.attron(curses.A_BOLD)
        self.screen.stdscr.attron(self.screen.curses_color_pairs['black_on_red'])
        if (self.screen.marky_bot.wss.conn.connected.is_set()):
            self.screen.stdscr.attron(self.screen.curses_color_pairs['black_on_green'])
        self.screen.stdscr.addstr(self.screen.height-1, (self.screen.width-5-len(exchange_connection)), "  {}  ".format(exchange_connection))
        self.clear_attrs()

        self.screen.stdscr.attron(self.screen.curses_color_pairs['black_on_white'])
        self.screen.stdscr.addstr(self.screen.height-1, 0, statusbarstr)

        self.screen.stdscr.addstr(self.screen.height-1, len(statusbarstr), " " * (self.screen.width - len(statusbarstr) - len(exchange_connection) - 5))
        self.clear_attrs()

    def move_cursor(self, **kwargs):
        """Move cursor around on screen."""
        self.screen.stdscr.move(self.screen.cursor_y, self.screen.cursor_x)
