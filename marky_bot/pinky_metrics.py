"""Use the pinky exchange and get metrics such as tape and orderbook."""
# from .pinky_timer import pinky_timer
import asyncio
from collections import defaultdict
from datetime import datetime
import time


class pinky_metrics:
    """Metricate."""

    def __init__(self, marky_bot_obj):
        """Initialize the metrics class.

        param: exchange - the exchange object that we're going to poke and prod here
        """
        self.marky_bot = marky_bot_obj
        self.marky_bot.metrics = self
        self.marky_bot.logger.info('Metrics started')
        self.init_triggers()

    def init_triggers(self):
        #  triggers are adde to the q when it's created, not all of them need to be used
        self._trade_q_triggers = {'log_total_volume': self.print_total_volume_in_logger,
                                  'render_last_trade': self.render_last_trade,
                                  'render_volume_stats': self.render_volume_stats,
                                  'render_trades_per_second': self.render_trades_per_second
                                  }
        #  All the metrics are run by default when a trade is registered
        self._trade_q_metrics = {'total_volume': self.total_volume,
                                 'total_sell_volume': self.total_sell_volume,
                                 'total_buy_volume': self.total_buy_volume,
                                 'total_volume_spread': self.total_volume_spread,
                                 'last_buy': self.last_buy,
                                 'last_sell': self.last_sell,
                                 'tape_price_spread': self.tape_price_spread,
                                 'log_trade_to_file': self.log_trade_to_file,
                                 'trades_per_second': self.trades_per_second_cache,
                                 'volume_per_second': self.volume_per_second_cache
                                 }

    async def monitor_trades_q(self, pair):
        #  self.queues = {'trades': {'BTCUSD': {'q_object: 'self.wss.trades('BTCUSD'), 'monitor': True, 'last_trade': [], 'triggers': [], 'metrics:' {}]}}}}
        self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Started monitoring Q'}
    #    self.marky_bot.logger.info(self.marky_bot.exchange_queues['trades'][pair])
        try:
            q = self.marky_bot.exchange_queues['trades'][pair[1]]
            self.marky_bot.logger.info('Begin monitoring trading Q: {}'.format(pair[0]))

        except Exception:
            self.marky_bot.logger.error('Trading Q: {} not found'.format(pair[1]))
            return

#        default_metrics = defaultdict(int)
#        metrics = {'metrics': default_metrics}
        self.marky_bot.exchange.empty_queue(q['q_object'])
        q['metrics'] = defaultdict(int)
        q['metrics']['volume_stats'] = defaultdict(int)
        q['metrics']['last_sell'] = [0, 0, 0, 0, 0]
        q['metrics']['last_buy'] = [0, 0, 0, 0, 0]
        q['metrics']['volume_stats']['total_buy_volume'] = int(0)
        q['metrics']['volume_stats']['total_sell_volume'] = int(0)
        q['metrics']['volume_stats']['total_volume'] = int(0)
        q['metrics']['num_of_trades'] = 0
        q['metrics']['trade_per_second'] = 0
        q['metrics']['last_fill'] = 0
        q['metrics']['indicators'] = {}
        self.marky_bot.timer.periodic_tasks['1s'].update({'trades_per_second': (self.trades_per_second_cache, q)})
        self.marky_bot.timer.periodic_tasks['1s'].update({'volume_per_second': (self.volume_per_second_cache, q)})
        self.marky_bot.timer.periodic_tasks['1s'].update({'55_ma': (self.ma_55, q)})
        self.marky_bot.timer.periodic_tasks['10m'].update({'clear_10m_trade_cache': (self.clear_trade_cache, q)})
        q['cache'] = []

        while (q['monitor']):
            if (q['q_object'].empty() is not True):
                last_trade = q['q_object'].get()
                try:
                    q['metrics']['last_trade'] = (last_trade[0][1])
                    q['metrics']['num_of_trades'] = q['metrics']['num_of_trades'] + 1
                    q['metrics']['last_trade'].append(time.time())
                    q['cache'].append(q['metrics']['last_trade'])
                except Exception:
                    q['metrics']['last_trade'] = (0, 0, 0, 0, 0)
                for mertic_name, metric in self._trade_q_metrics.items():
                    try:
                        metric(q)
                    except Exception:
                        raise
                        pass

                for trigger in q['triggers']:
                    try:
                        self._trade_q_triggers[trigger](q)
                    except Exception:
                        raise
                        pass
                self.marky_bot.order_manager.match_orders_to_trades(pair, q['metrics']['last_trade'])

            await asyncio.sleep(0.05)

    def clear_trade_cache(self, q):
        now = int(time.time())
        interval = 360 #  I know ityeah, that's 's not ten minutes
        purged = 0
        for trade in q['cache']:
            if (int(trade[4]) <= (now-interval)):
                del trade
                purged += 1
        self.marky_bot.logger.info('Cleared 10 minute trades cache. Purged: {} Remaining: {}'.format(purged, len(q['cache'])))

    def ma_55(self, q):
        now = int(time.time())
        length = 300
        num_of_trades = 0
        price = 0
        ma = 0
        for trade in q['cache']:
            if (int(trade[4]) >= (now-length)):
                num_of_trades += 1
                price += trade[3]
                ma = price / num_of_trades

        q['metrics']['indicators']['55ma'] = ma

    def volume_per_second_cache(self, q):
        now = int(time.time())
        interval = 300
        per_interval = int(interval)
        vps = 0
        svps = 0
        bvps = 0
        oldest_trade_age = now
        for trade in q['cache']:
            if (int(trade[4]) >= (now-interval)):
                vps += abs(trade[2])
                if (trade[2] > 0):
                    bvps += trade[2]
                elif (trade[2] < 0):
                    svps += trade[2]

                if (int(trade[4]) < oldest_trade_age):
                    oldest_trade_age = int(trade[4])
                    if (per_interval > now - oldest_trade_age):
                        per_interval = now - oldest_trade_age

        q['metrics']['volume_per_second'] = vps / per_interval
        q['metrics']['buy_volume_per_second'] = bvps / per_interval
        q['metrics']['sell_volume_per_second'] = svps / per_interval

    def trades_per_second_cache(self, q):
        now = int(time.time())
        interval = 300
        per_interval = int(interval)
        tps = 0
        sps = 0
        bps = 0
        oldest_trade_age = now
        for trade in q['cache']:
            if (int(trade[4]) >= (now-interval)):
                tps += 1
                if (trade[2] > 0):
                    bps += 1
                elif (trade[2] < 0):
                    sps += 1

                if (int(trade[4]) < oldest_trade_age):
                    oldest_trade_age = int(trade[4])
                    if (per_interval > now - oldest_trade_age):
                        per_interval = now - oldest_trade_age

        q['metrics']['trades_per_second'] = tps / per_interval
        q['metrics']['buys_per_second'] = bps / per_interval
        q['metrics']['sells_per_second'] = sps / per_interval

    def get_last_price(self, pair):
        try:
            last_price = self.marky_bot.exchange_queues['trades'][pair[1]]['metrics']['last_trade'][3]
        except Exception:
            last_price = 0.0
            self.marky_bot.logger.error('No data on {} pair queue'.format(pair[1]))
            pass

        return last_price

    def print_total_volume_in_logger(self, q):
        self.marky_bot.logger.info('{:.4f}'.format(q['metrics']['volume_stats']['total_volume']))

    def tape_price_spread(self, q):
        q['metrics']['tape_price_spread'] = q['metrics']['last_sell'][3] - q['metrics']['last_buy'][3]
        try:
            q['metrics']['tape_price_spread_percentage'] = 100 * (q['metrics']['tape_price_spread'] / ((q['metrics']['last_sell'][3] - q['metrics']['last_buy'][3])/2))
        except ZeroDivisionError:
            q['metrics']['tape_price_spread_percentage'] = int(0)

    def last_sell(self, q):
        if (q['metrics']['last_trade'][2] < 0):
            q['metrics']['last_sell'] = q['metrics']['last_trade']

    def last_buy(self, q):
        if (q['metrics']['last_trade'][2] > 0):
            q['metrics']['last_buy'] = q['metrics']['last_trade']

    def total_volume_spread(self, q):
        q['metrics']['volume_stats']['total_volume_spread'] = q['metrics']['volume_stats']['total_buy_volume'] + q['metrics']['volume_stats']['total_sell_volume']
        try:
            q['metrics']['volume_stats']['total_volume_spread_percentage'] = 100 * (q['metrics']['volume_stats']['total_volume_spread'] / q['metrics']['volume_stats']['total_volume'])
        except ZeroDivisionError:
            q['metrics']['volume_stats']['total_volume_spread_percentage'] = int(0)
            pass

    def total_buy_volume(self, q):
        if (q['metrics']['last_trade'][2] > 0):
            q['metrics']['volume_stats']['total_buy_volume'] = q['metrics']['volume_stats']['total_buy_volume'] + q['metrics']['last_trade'][2]

    def total_sell_volume(self, q):
        if (q['metrics']['last_trade'][2] < 0):
            q['metrics']['volume_stats']['total_sell_volume'] = q['metrics']['volume_stats']['total_sell_volume'] + q['metrics']['last_trade'][2]

    def total_volume(self, q):
        q['metrics']['volume_stats']['total_volume'] = q['metrics']['volume_stats']['total_volume']+abs(q['metrics']['last_trade'][2])

    def log_trade_to_file(self, q):
        self.marky_bot.trade_logger.info('{};{};{};{};{}'.format(q['metrics']['last_trade'][0], q['metrics']['last_trade'][2], q['metrics']['last_trade'][3], q['metrics']['last_trade'][1],q['metrics']['last_trade'][4]))

    def render_last_trade(self, q):
        self.marky_bot.curses_wrapper.screen.buffer['render_last_trade'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.last_trade, 'params': {'last_trade': q['metrics']['last_trade']}}

    def render_volume_stats(self, q):
        self.marky_bot.curses_wrapper.screen.buffer['render_volume_stats'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.volume_stats, 'params': {'volume_stats': q['metrics']['volume_stats']}}

    def render_trades_per_second(self, q):
        self.marky_bot.curses_wrapper.screen.buffer['render_trade_per_second'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.trades_per_second, 'params': {'metrics': q['metrics']}}
