"""Just gonna put some exchange talkie talkie stuff here."""
# import things we needz
import time


class pinky_exchange():
    """Get's go.

    very helpful huh.
    """

    def __init__(self, marky_bot_obj):
        """Initialize the websockets echange stuff.

        Initialize logging and creates the exchange object.
        NOTE: does not connect just yet
        """
        self.marky_bot = marky_bot_obj
        self.marky_bot.exchange = self

        self.marky_bot.exchange_queues = {}

        self.PAIRS = {
                'BTC': {'USD': ('BTC/USD', 'BTCUSD', ('BTC', 'USD'))},
                'ETH': {'USD': ('ETH/USD', 'ETHUSD', ('ETH', 'USD'))},
                'EOS': {'USD': ('EOS/USD', 'EOSUSD', ('EOS', 'USD'))}
                }
        self.MARKETS = ('exchange', 'margin')

    def connect(self):
        """Make a connection to the exchange."""
        self.marky_bot.wss.start()
        while not self.marky_bot.wss.conn.connected.is_set():
            time.sleep(1)
        self.subscribe_to_trades(self.PAIRS['BTC']['USD'])

    def disconnect(self):
        """Disconnect form the exchange."""
#        self.marky_bot.exchange_queues['trades']['BTCUSD']['monitor'] = False
        self.marky_bot.wss.unsubscribe_from_trades(self.PAIRS['BTC']['USD'][1])
        self.marky_bot.wss.stop()

    def subscribe_to_trades(self, pair):
        self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Subscribing...'}
        attempts = 0
        while self.marky_bot.wss.trades(pair[1]).empty() and attempts < 3:
            attempts = attempts + 1
            self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Waiting to subscribe {}...'.format(attempts)}
            self.marky_bot.wss.subscribe_to_trades(pair[1])
            time.sleep(1)
        if attempts >= 3:
            self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Failed to subscribe. Too many tries'}
            return
        self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Subscribed.'}
        self.marky_bot.exchange_queues['trades'] = {pair[1]: {'q_object': self.marky_bot.wss.trades(pair[1]), 'monitor': True, 'triggers': ['render_last_trade', 'render_volume_stats', 'render_trades_per_second'], 'metrics': {}}}
        self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Q created'}
        self.marky_bot.asyncio_q.put(self.marky_bot.metrics.monitor_trades_q(pair))
        self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': 'Metrics task adde'}

    def unsubscribe_from_trades(self, pair):
        self.marky_bot.wss.unsubscribe_from_trades(pair[1])
        self.marky_bot.exchange_queues['trades'][pair[1]]['monitor'] = False
        #self.queues = {'trades': {'BTCUSD': {'q_object': self.marky_bot.exchange.wss.trades('BTCUSD'), 'monitor': True, 'last_trade': []}}}
        #self.marky_bot.metrics.monitor_trades_q('BTCUSD')
        #self.marky_bot.curses_wrapper.screen.buffer['render_title']['params'] = {'title_text': self.marky_bot.exchange.queues['trades']['BTCUSD']['last_trade']}

    def empty_queue(self, queue_obj):
        """Empty the given queue."""
        while (not queue_obj.empty()):
            try:
                queue_obj.get(True)
            except Exception:
                pass
