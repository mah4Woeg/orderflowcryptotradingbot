"""Tasks that any and all classes may use.

Static class.
"""


class common_tasks:
    def determine_sign(var):
        try:
            return var / abs(var)
        except Exception:
            pass
            return 0

    def clamp_value_to_max(value, max, min=0):
        return min(max, max(min, value))
