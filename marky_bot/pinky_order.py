"""Just the stuff an order can do.

And the stuff that can be done to it, hehe
"""
import time
from .common_tasks import common_tasks


class order:
    """I am an order, hear me ,,,, bllurg."""

    STATUSES = {
               0: 'new',
               1: 'cancelled',
               2: 'active',
               3: 'filled',
               4: 'partially_filled',
               5: 'rejected',
               6: 'incomplete'
               }


    def __init__(self, order_manager, type, pair, market, price, quantity):
        """Initialize a new order.

        params: order_manager, pair, market, price, quantity
        needs to be activated bofore if can be executed
        """
        self.order_manager = order_manager
        self.logger = self.order_manager.logger

        #  Lets setup some variables
        self._type = type
        self._pair = pair
        self._market = market
        self._price = price
        self._quantity = quantity
        self._cost = 0
        self._fill = 0
        self._side = self._set_side()
        self._id = self._gen_id()
        self._est_cost = self._calculate_estimated_cost()
        self._status = {order.STATUSES[0]}

    def activate(self):
<<<<<<< HEAD
        """Complete this function all proper like.
        add balance validation
        """
=======
        """Complete this function all proper like."""

        #  check that there is enough balance to activate this order_manager
        #  use frozen margin per order (can be positive can be negative depending on offer type)
        #  if all checks out remove the "new" status and add the active status

>>>>>>> added comment to the order activate for what needs to be done
        try:
            self._status.remove(self.STATUSES[0])
        except Exception:
            pass

        self._status.add(self.STATUSES[1])

    def cancel(self):
        if (self.STATUSES[2] in self.status):
            self._status.remove(self.STATUSES[2])
            self._status.add(self.STATUSES[1])

    def _fill_margin_limit_order(self, trade):
        if (self.side[1] + common_tasks.determine_sign(trade[2]) == 0):
            return False
        elif (self.STATUSES[2] not in self.status):
            return False
        try:
            self._status.remove(self.STATUSES[0])
        except Exception:
            pass
        trade_quantity = abs(trade[2])
        quantity_to_add = common_tasks.clamp_value_to_max(trade_quantity, self.quantity - self.fill)
        cost_to_add = quantity_to_add * self.price()
        if (self.order_manager.request_margin_funds(self, cost_to_add)):
            trade[2] -= quantity_to_add * common_tasks.determine_sign(trade[2])
            self._fill += quantity_to_add
            self._cost += cost_to_add

            self._status.add(self.STATUSES[4])
            if self.fill == self.quantity:
                self._status.remove(self.STATUSES[4])
                self._status.remove(self.STATUSES[2])
                self._status.add(self.STATUSES[3])
        else:
            self.logger.error('Insufficient funds to fill order {} for quantity {} cost {}'.format(self.id, quantity_to_add, cost_to_add))

    def _gen_id(self):
        try:
            id = '{}-{}-{}'.format(self.type, self._pair[0], time.time())
        except Exception as e:
            self.logger.error('Cannot generate orrder id: {}'.format(e))
            pass
            return False

        return id

    def _calculate_estimated_cost(self):
        return self.price * self.quantity

    def _set_side(self):
        try:
            side = (side for side in self.order_manager.SIDES if common_tasks.determine_sign(self.quantity) in side)
            side = list(side)
        except Exception as e:
            self.logger.error('Cannot set side on order: {}'.format(e))
            pass
            return False

        self._quantity = abs(self.quantity)
        return side[0]

    @property
    def _last_price(self):
        try:
            last_price = self.order_manager.marky_bot.metrics.get_last_price(self.pair[1])
        except Exception as e:
            last_price = 0.0
            self.logger.error('Order : {} - No data on {} pair queue: {}'.format(self.id, self.pair[1], e))
            pass

        return last_price

    @property
    def id(self):
        return self._id

    @property
    def price(self):
        return self._price

    @property
    def quantity(self):
        return self._quantity

    @property
    def market(self):
        return self._market

    @market.setter
    def market(self, market_to_set):
        try:
            self._market = (market for market in self.order_manager.MARKETS if market_to_set in self.order_manager.MARKETS)
        except Exception as e:
            self.logger.error('Cannot set market on order: {}'.format(e))
            self._status.add(self.STATUSES[6])
            pass
            return False

    @property
    def pair(self):
        return self._pair

    @property
    def type(self):
        return self._type

    @market.setter
    def market(self, type_to_set):
        try:
            self._type = (type for type in self.order_manager.TYPES if type_to_set in self.order_manager.TYPES)
        except Exception as e:
            self.logger.error('Cannot set type on order: {}'.format(e))
            self._status.add(self.STATUSES[6])
            pass
            return False

    @property
    def side(self):
        return self._side

    @property
    def status(self):
        return self._status

    @property
    def fill(self):
        return self._fill

    @fill.setter
    def fill(self, trade):
        fill_handler_name = '_fill_'+self.market[1]+'_'+self.type[1]+'_order'
        try:
            fill_handler = getattr(self, fill_handler_name)
            fill_handler(trade)
        except Exception as e:
            self.logger.error('Cannot call order fill method ({}): {}'.format(fill_handler_name, e))
            pass

    @property
    def cost(self):
        return self._cost

    @property
    def estimated_cost(self):
        return self._est_cost
