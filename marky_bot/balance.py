"""Keeps track of all the balances."""


class balance:

    def __init__(self, marky_bot_obj, init_balance={'USD': 0}):
        """Initialize balance management classself.

        param: marky_bot_obj, initial balances
        """
        self.marky_bot = marky_bot_obj
        self.marky_bot.balance = self
        self.margin = margin_balance(marky_bot_obj)

        self.MAKER_FEE = 0.1  # default bitfinex maker fee
        self.TAKER_FEE = 0.2  # default bitfinex taker fee

        self._available_assets = init_balance

        self.marky_bot.curses_wrapper.screen.buffer['render_balances'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.balances}
   
    def add_balance(self, balance_to_add):
        try:
            for symbol, quantity in balance_to_add.items():
                self._available_assets[symbol] += abs(quantity)
        except Exception as e:
            self.marky_bot.logger.error('Cannot add to balance: {}'.format(e))

    def remove_balance(self, balance_to_sub):
        try:
            for symbol, quantity in balance_to_sub.items():
                self._available_assets[symbol] = max(0, self._available_assets[symbol] - abs(quantity))
        except Exception as e:
            self.marky_bot.logger.error('Cannot add to balance: {}'.format(e))

    @property
    def available_assets(self):
        return self._available_assets


class margin_balance:

    def __init__(self, marky_bot_obj):
        self.marky_bot = marky_bot_obj
        self.balance = self.marky_bot.balance
        self.MARGIN_LEVERAGE = 3.33  # default bitfinex margin leverage

    def add_margin(self, margin_to_add):
        try:
            for symbol, quantity in margin_to_add.items():
                self.balance.add_balance({symbol: abs(quantity / self.MARGIN_LEVERAGE)})
        except Exception as e:
            self.marky_bot.logger.error('Cannot add to margin: {}'.format(e))

    def substract_margin(self, margin_to_sub):
        try:
            for symbol, quantity in margin_to_sub.items():
                self.balance.remove_balance({symbol: abs(quantity / self.MARGIN_LEVERAGE)})
        except Exception as e:
            self.marky_bot.logger.error('Cannot remove to margin: {}'.format(e))

    @property
    def tradable_margin(self):
        return {symbol: quantity * self.MARGIN_LEVERAGE for symbol, quantity in self.balance.available_assets.items()}
