"""Takes care of screen loops and keyboard input."""

import curses
import asyncio
import queue
from .screen_tasks import screen_tasks


class pinky_screen:
    """This is the screen object to be shared and passed around, hehe."""

    def __init__(self, marky_bot_obj):
        """Initialize the screen object.

        params: marky_bot - main bot class instance (passed from the curses wrapper)
        """
        self.marky_bot = marky_bot_obj
        self.refresh_delay = 0.05  # Sets the screen refresh rate
        self.buffer = {}  # Things to be output on the sceen go here
        # Screen properties
        self.height = 0
        self.width = 0
        self.cursor_x = 0
        self.cursor_y = 0
        self.stdscr = curses.initscr()

        self.tasks = screen_tasks(self)
        curses.noecho()
        curses.cbreak()
        self.stdscr.keypad(True)
        self.stdscr.nodelay(True)

        self.init_curses_color_pairs()

    def init_curses_color_pairs(self):
        """Create an array of text/beckground colors for use with curses."""
        curses.start_color()
        #  define the color pairs
        curses.init_pair(1, curses.COLOR_CYAN, curses.COLOR_BLACK)
        curses.init_pair(2, curses.COLOR_RED, curses.COLOR_BLACK)
        curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
        curses.init_pair(4, curses.COLOR_BLACK, curses.COLOR_WHITE)
        curses.init_pair(5, curses.COLOR_BLACK, curses.COLOR_GREEN)
        curses.init_pair(6, curses.COLOR_BLACK, curses.COLOR_RED)
        curses.init_pair(7, curses.COLOR_WHITE, curses.COLOR_BLACK)

        #  add color pairs to the dict so we can easily access them when we need
        self.curses_color_pairs = {'cyan_on_black': curses.color_pair(1),
                                   'red_on_black': curses.color_pair(2),
                                   'green_on_black': curses.color_pair(3),
                                   'black_on_white': curses.color_pair(4),
                                   'black_on_green': curses.color_pair(5),
                                   'black_on_red': curses.color_pair(6),
                                   'white_on_black': curses.color_pair(7)}

    def cleanup(self):
        """Run cleanup, return the terminal to initial state."""
        self.stdscr.nodelay(False)
        self.stdscr.keypad(False)
        curses.nocbreak()
        curses.echo()
        curses.endwin()


class pinky_curses_wrapper:
    """Curses wrapperself.

    Outputs stuff on screen and takes keyboard input.
    acts on keyboard input.
    """

    def __init__(self, marky_bot_obj):
        """Initialize the screen.

        params: marky_bot - the main bot class instance
        Cheezit
        """
        self.marky_bot = marky_bot_obj
        self.last_key_pressed = 0  # keeps the last non zero value key pressed
        self.current_key_pressed = 0  # same as last key, but may be zero
        self.input_delay = 0.01  # Sets the asyncio wait time for keyboard input
        self.debug_stream = ''
        self.screen = pinky_screen(marky_bot_obj)
#        self.init_keyboard_interrupts()
        self.inputq = queue.Queue()
        self.key_handler = ''
        self.sell_order_id = []
        self.buy_order_id = []

    def cleanup(self):
        """Clean up."""
        self.screen.cleanup()

    async def render_screen(self):
        """Render screen output."""
        # Clear and refresh the screen before we start
        self.screen.stdscr.clear()

        # Keep outputting stuff until stopped
        while (self.marky_bot.continue_loops is True):
            # Initialization
            self.screen.height, self.screen.width = self.screen.stdscr.getmaxyx()
            curses.curs_set(1)

            self.screen.cursor_x = max(0, self.screen.cursor_x)
            self.screen.cursor_x = min(self.screen.width-1, self.screen.cursor_x)

            self.screen.cursor_y = max(0, self.screen.cursor_y)
            self.screen.cursor_y = min(self.screen.height-1, self.screen.cursor_y)

            # Screen output goes where
            # self.screen.tasks.title()
            self.screen.stdscr.erase()
            for task_name, task in self.screen.buffer.items():
                try:
                    task['obj'](**task['params'])
                except KeyError:
                    task['obj']()
                    pass

            self.screen.tasks.status_bar()
            self.screen.tasks.move_cursor()
            self.screen.stdscr.refresh()
            await asyncio.sleep(self.screen.refresh_delay)
        self.screen.cleanup()

    async def cap_input(self):
        """Run continueously capturing keyboard inputs.

        cycle thorugh the interrupts dict and run any functions that match
        """
        while (self.marky_bot.continue_loops is True):
            current_key_pressed = self.screen.stdscr.getch()
            if current_key_pressed > 0:
                self.inputq.put(current_key_pressed)
            await asyncio.sleep(self.input_delay)

    async def handle_input(self):
        while (self.marky_bot.continue_loops is True):
            if (self.inputq.empty() is not True):
                key_pressed = self.inputq.get()
                handler_name = '_handle_{}_key'.format(key_pressed)
                self.marky_bot.logger.info('Key pressed {}'.format(key_pressed))
                try:
                    handler = getattr(self, handler_name)
                    handler()
                except Exception as e:
                    self._handle_default_key
                    pass

            await asyncio.sleep(self.input_delay)
    # Key key_handlers

    def _handle_259_key(self):  # Up key
        self.screen.cursor_y = self.screen.cursor_y - 1

    def _handle_258_key(self):  # down key
        self.screen.cursor_y = self.screen.cursor_y + 1

    def _handle_260_key(self):  # left key
        self.screen.cursor_x = self.screen.cursor_x - 1

    def _handle_261_key(self):  # right key
        self.screen.cursor_x = self.screen.cursor_x + 1

    def _handle_default_key(self):  # default key
        return
        #  self.debug_stream = "Local key {}".format(self.last_key_pressed)
        #  self.marky_bot.debug_stream = "Pressed any key {}".format(self.last_key_pressed)

    def _handle_116_key(self):  # t key
        self.screen.buffer['render_title']['params'] = {'title_text': 'Fuck off'}

    def _handle_99_key(self):  # c key
        self.marky_bot.logger.info('Connecting to exchage...')
        self.marky_bot.exchange.connect()
        self.marky_bot.logger.info('Connected to exchange. Now to subscribe.')

    def _handle_100_key(self):  # d key
        self.marky_bot.logger.info('Disconnecting from exchage...')
        self.marky_bot.exchange.disconnect()

    def _handle_115_key(self):  # s key
        self.marky_bot.exchange.subscribe_to_trades('BTCUSD')
        self.marky_bot.logger.info('Subscribed.')

    def _handle_117_key(self):  # u key
        #  self.marky_bot.exchange.unsubscribe_from_trades('BTCUSD')
        self.marky_bot.logger.info(self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['indicators']['55ma'])

    def _handle_110_key(self):  # n key
        self.marky_bot.logger.info('Creating margin limit buy order')
        try:
            ema = self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['indicators']['55ma']
            spread = self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['tape_price_spread']
            last_price = self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['last_trade'][3]
        except Exception:
            ema = 0
            spread = 0
            last_price = 0

        try:
            self.marky_bot.order_manager.new_order(self.marky_bot.order_manager.TYPES[0], self.marky_bot.exchange.PAIRS['BTC']['USD'], self.marky_bot.order_manager.MARKETS[1], 6000, -1.4)
            self.marky_bot.logger.info('Created order: {}'.format(self.buy_order_id))
        except Exception as e:
            self.marky_bot.logger.info('Failed to create order: {}'.format(e))
            pass

    def _handle_109_key(self):  # m key
        self.marky_bot.trader.cancel_order(self.buy_order_id[-1])
        del self.buy_order_id[-1]
        self.marky_bot.logger.info('Cancelled order: {}'.format(self.buy_order_id))

    def _handle_106_key(self):  # j key
        self.marky_bot.logger.info('Creating margin limit sell order')
        try:
            ema = self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['indicators']['55ma']
            spread = self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['tape_price_spread']
            last_price = self.marky_bot.exchange_queues['trades']['BTCUSD']['metrics']['last_trade'][3]
        except Exception:
            ema = 0
            spread = 0
            last_price = 0

        try:
            self.marky_bot.order_manager.new_order(self.marky_bot.order_manager.TYPES[0], self.marky_bot.exchange.PAIRS['BTC']['USD'], self.marky_bot.order_manager.MARKETS[1], 6000, 1.4)
            self.marky_bot.logger.info('Created order: {}'.format(self.buy_order_id))
        except Exception as e:
            self.marky_bot.logger.info('Failed to create order: {}'.format(e))
            pass

    def _handle_107_key(self):  # k key
        self.marky_bot.trader.cancel_order(self.sell_order_id[-1])
        del self.sell_order_id[-1]
        self.marky_bot.logger.info('Cancelled order: {}'.format(self.sell_order_id))

    def _handle_43_key(self):  # + key
        self.marky_bot.balance.add_balance({'USD': 1000})

    def _handle_95_key(self):  # - key
        self.marky_bot.balance.remove_balance({'USD': 1000})

    def _handle_114_key(self):  # r key
        self.screen.stdscr.clear()

    def _handle_113_key(self):  # q key
        self.marky_bot.continue_loops = False
        self.marky_bot.exchange.disconnect()
        self.cleanup()
