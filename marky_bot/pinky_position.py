"""Just the stuff a position can do.

And the stuff that can be done to it, hehe
"""
import time
from .common_tasks import common_tasks


class position:
    """I am a position, watch me position myself."""

    LIQUIDATION_PERCENTAGE = 15
    SIDES = (('short', -1), ('long', 1))
    STATUSES = {
               0: 'new',
               1: 'market_closed',
               2: 'limit_closed',
               3: 'open'
               }

    def __init__(self, order_manager, pair, quantity, cost):
        """Initialize a new order.

        params: order_manager, pair, quantity, cost
        """
        self.order_manager = order_manager
        self.logger = self.order_manager.logger

        #  Lets setup some variables
        self._pair = pair
        self._quantity = quantity
        self._cost = cost
        self._current_cost
        self._base_price
        self._fee
        self._liquidation_price
        self._side = self._set_side()
        self._id = self._gen_id()
        self._status = {position.STATUSES[0]}

    def _gen_id(self):
        try:
            id = '{}-{}-{}'.format(self.type, self._pair[1], time.time())
        except Exception as e:
            self.logger.error('Cannot generate position id: {}'.format(e))
            pass
            return False

        return id

    def _set_side(self):
        try:
            side = (side for side in self.SIDES if common_tasks.determine_sign(self.quantity) in side[1])
        except Exception as e:
            self.logger.error('Cannot set side on position: {}'.format(e))
            pass
            return False

        self._quantity = abs(self.quantity)
        return side

    @property
    def id(self):
        return self._gen_id

    @property
    def price(self):
        return self._price

    @property
    def quantity(self):
        return self._quantity

    @property
    def pair(self):
        return self._pair

    @property
    def side(self):
        return self._side

    @property
    def status(self):
        return self._status

    @quantity.setter
    def quantity(self, order):
        """Complete this oneself.

        needs to be able to reverse th eposition.
        """

    @property
    def cost(self):
        return self._cost

    @property
    def current_cost(self):
        return self._current_cost
