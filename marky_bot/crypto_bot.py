"""The main marky_bot class."""
from marky_bot.pinky_curses_wrapper import pinky_curses_wrapper as curses_wrapper
from marky_bot.pinky_timer import pinky_timer as timer
from marky_bot.pinky_metrics import pinky_metrics as metrics
from marky_bot.pinky_exchange import pinky_exchange as exchange
#from marky_bot.pinky_trader import pinky_trader as trader
from marky_bot.balance import balance
from marky_bot.order_manager import order_manager
from logging.handlers import RotatingFileHandler

from btfxwss import BtfxWss
import logging
import io
import asyncio
import queue


class Marky_Bot:
    """That's the head huncho."""

    def __init__(self):
        """Initialize the marky_bot.

        params: none
        """
        self.init_logging()

        self.curses_wrapper = curses_wrapper(self)
        self.wss = BtfxWss()
        self.exchange = exchange(self)
        self.metrics = metrics(self)
        self.timer = timer(self)
        self.balance = balance(self)
        self.order_manager = order_manager(self)
#        self.trader = trader(self)

        self.asyncio_loop = asyncio.get_event_loop()
        self.asyncio_tasks = {}

        self.asyncio_q = queue.Queue()

        self.asyncio_q.put(self.curses_wrapper.render_screen())
        self.asyncio_q.put(self.curses_wrapper.cap_input())
        self.asyncio_q.put(self.curses_wrapper.handle_input())
        self.asyncio_q.put(self.timer.do_every_10_minutes())
        self.asyncio_q.put(self.timer.do_every_1_second())
        #self.asyncio_q.put(self.trader.watch_margin_balance())
        #self.asyncio_q.put(self.trader.update_margin_pl())


#        self.asyncio_q.put(self.timer.do_every_x_seconds(time_table='10m', t=3600, i=600, tl_id='1h/10m'))
        self.continue_loops = True

    def init_logging(self):
        self.debug_stream = io.StringIO()
        self.root_debug_stream = io.StringIO()
        self.logger = logging.getLogger(__name__)
        self.trade_logger = logging.getLogger('trade_logger')

        bot_file_formatter = logging.Formatter('%(asctime)s: %(levelname)s - %(name)s:%(message)s')
        root_file_formatter = logging.Formatter('%(asctime)s: %(levelname)s - %(name)s:%(message)s')
        trade_file_formatter = logging.Formatter('%(asctime)s:%(message)s')

        root_file_handler = RotatingFileHandler('marky_bot_root_debug.log', maxBytes=1048576, backupCount=3)
        bot_file_handler = logging.FileHandler('marky_bot_debug.log')
        trade_file_handler = logging.FileHandler('trades.log')

        root_file_handler.setLevel(logging.WARNING)
        bot_file_handler.setLevel(logging.DEBUG)
        trade_file_handler.setLevel(logging.INFO)

        root_file_handler.setFormatter(root_file_formatter)
        bot_file_handler.setFormatter(bot_file_formatter)
        trade_file_handler.setFormatter(trade_file_formatter)

        self.trade_logger.addHandler(trade_file_handler)
        self.logger.addHandler(bot_file_handler)

        stream_formatter = logging.Formatter('%(message)s')

        root_stream_handler = logging.StreamHandler(self.root_debug_stream)
        root_stream_handler.setLevel(logging.INFO)
        root_stream_handler.setFormatter(stream_formatter)

        bot_stream_handler = logging.StreamHandler(self.debug_stream)
        bot_stream_handler.setLevel(logging.INFO)
        bot_stream_handler.setFormatter(stream_formatter)

        self.logger.addHandler(bot_stream_handler)

        logging.basicConfig(level=logging.INFO, handlers=[root_stream_handler, root_file_handler])

        self.logger.info('Logging initialized.')

    async def asyncio_work(self):
        while self.continue_loops:
            if (self.asyncio_q.empty() is not True):
                asyncio.gather(self.asyncio_q.get())
            await asyncio.sleep(.1)

    def start_loop(self):
        self.asyncio_loop.set_debug(1)
        self.asyncio_loop.run_until_complete(self.asyncio_work())
        self.asyncio_loop.close()
