"""Managing orders ain't easy.

But this one will do it's best to wrangle all of them unruly orders.
"""

import logging
from .common_tasks import common_tasks
from .pinky_order import order as order_object
from .pinky_position import position as position_object
from logging.handlers import RotatingFileHandler


class order_manager:
    """I am the order master, orders will obey."""

    SIDES = (('ask', -1), ('bid', 1))

    MARKETS = {
              0: 'exchange',
              1: 'margin'
              }

    TYPES = {
                 0: 'limit',
                 1: 'market',
                 2: 'stop'
                 }

    def __init__(self, marky_bot_obj):
        """Initialize order manager.

        params: order_manager, pair, market, price, quantity
        needs to be activated bofore if can be executed
        """
        self.marky_bot = marky_bot_obj
        self.marky_bot.order_manager = self
        self.orders = set()
        self.positions = set()

        self.init_logging()

        self.marky_bot.curses_wrapper.screen.buffer['render_orders'] = {'obj': self.marky_bot.curses_wrapper.screen.tasks.orders}

    def init_logging(self):
        self.logger = logging.getLogger(__name__)

        order_manager_file_formatter = logging.Formatter('%(asctime)s: %(levelname)s - %(name)s:%(message)s')
        order_manager_file_handler = RotatingFileHandler('order_manager_debug.log', maxBytes=1048576, backupCount=3)

        order_manager_file_handler.setLevel(logging.INFO)
        order_manager_file_handler.setFormatter(order_manager_file_formatter)

        self.logger.addHandler(order_manager_file_handler)

        self.logger.info('Order manager logging initialized.')

    def match_orders_to_trades(self, pair, trade):
        active_limit_orders = self.get_active_limit_orders(pair)

        for active_limit_order in active_limit_orders:
            active_limit_order.fill = trade

    def new_order(self, type, pair, market, price, quantity):
        new_order = order_object(self, type, pair, market, price, quantity)
        self.orders.add(new_order)
        self.logger.info('Order added {}'.format(new_order.id))

    def request_margin_funds(self, order, cost):
        return True if self.balance.margin.tradable_margin + self.match_position_balance_to_order(order) + self.margin_on_order(order) <= cost else False

    def margin_on_order(self, order=False):
        margin_on_order = 0.0
        quantity_on_order = 0.0
        open_orders = {open_order for open_order in self.orders if ((open_order.pair == order.pair if order is not False else True) and open_order.STATUSES[2] in open_order.status)}
        if (len(open_orders) > 0):
            for open_order in open_orders:
                margin_on_order += open_order.estimated_cost * open_order.side[1]
                quantity_on_order += open_order.quantity * open_order.side[1]
            margin_on_order = abs(margin_on_order)
        else:
            return 0.0

        if (quantity_on_order == 0.0 or order is False):
            return margin_on_order

        if (common_tasks.determine_sign(quantity_on_order) + order.side[1] == 0):
            return margin_on_order
        else:
            return 0

    def match_position_to_order(self, order):
        if (order is False):
            return False
        matched_position = (position for position in self.positions if position.pair == order.pair and position.STATUSES[2] in position.status)
        matched_position = list(matched_position)
        return matched_position[0] if len(matched_position) == 1 else False

    def match_position_balance_to_order(self, order=False):
        matched_position = self.match_position_to_order(order)
        if (matched_position is False):
            return 0.0

        if (matched_position.side[1] + order.side[1] == 0):
            return matched_position.cost
        else:
            return 0.0

    @property
    def get_active_limit_orders(self, pair):
        return {open_order for open_order in self.orders if (open_order.pair == pair and open_order.type == self.TYPES[0] and open_order.STATUSES[2] in open_order.status)}
