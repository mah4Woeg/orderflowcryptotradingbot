"""This one will do timing functionsself.

For example, generating periods in which to measure stuff.
Candles too, probably.
"""

import time
from datetime import datetime
import asyncio
from collections import defaultdict


class pinky_timer:

    def __init__(self, marky_bot_obj):

        self.marky_bot = marky_bot_obj
        self.marky_bot.timer = self

        self.zero_time = time.time()

        self.periodic_tasks = {'1s': {},
                               '30s': {},
                               '10m': {}
                               }

        self.run_time_loop = defaultdict(lambda: True)
        self.marky_bot.logger.info('Timer initialized')

    async def do_every_10_minutes(self):
        self.marky_bot.logger.info('Monitoring for every 10 minutes')
        n = int(60/10)
        while self.run_time_loop["1h/10m"]:
            mins = datetime.utcnow().minute
            if (mins in [x*10 for x in range(n)]):
                for task_name, task in self.periodic_tasks['10m'].items():
                    self.marky_bot.logger.info('Running 10m scheduled task: {}'.format(task_name))
                    task[0](task[1])
                await asyncio.sleep(100)

            await asyncio.sleep(30)

    async def do_every_1_second(self):
        self.marky_bot.logger.info('Monitoring for every 1 second')
        while self.run_time_loop['1m/1s']:
            secs = datetime.utcnow().second
            if (secs in [x for x in range(60)]):
                for task_name, task in self.periodic_tasks['1s'].items():
                    task[0](task[1])

            await asyncio.sleep(1)
