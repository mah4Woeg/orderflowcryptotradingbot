from marky_bot.crypto_bot import Marky_Bot as crypto_bot

import asyncio

markly_bot = crypto_bot()

# add a screen tasks to be displayed
markly_bot.curses_wrapper.screen.buffer['render_title'] = {'obj': markly_bot.curses_wrapper.screen.tasks.title, 'params': {'title_text': '\u25b2 Empty title for now \u25bc'}}

markly_bot.start_loop()
